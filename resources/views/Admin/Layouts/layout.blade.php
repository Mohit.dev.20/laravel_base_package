@include('Admin.Includes.header')
@include('Admin.Includes.navbar')
@include('Admin.Includes.sidebar')

@yield('content')

@include('Admin.Includes.footer')
@include('Admin.Includes.js-files')

@yield('script')

</body>

</html>
